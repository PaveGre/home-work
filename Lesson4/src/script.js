/*
Напиши функцию map(fn, array), которая принимает на вход функцию и массив,
 и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.
*/

function map(fn,arr){
    let newArr = [];
    for(let i = 0; i < arr.length; i++){
        newArr.push(square(arr[i]));
    }
    return newArr;
}
function square(x) {return x*x;}
console.log(map(square,[1,2,3,4]));
console.log(map(square,[]));

/*
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18.
В ином случае она задаёт вопрос confirm и возвращает его результат.
*/

//Используя оператор ?

function checkAge(age) {
    return (age > 18) ? true : confirm('Родители разрешили?');
}

//Используя оператор ||

function checkAge(age) {
    return (age > 18) || confirm('Родители разрешили?');
}
