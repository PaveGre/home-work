/*Создать объект «Документ», в котором определить свойства «Заголовок, Тело, Футер, Дата». Создать в объекте вложенный объект – «Приложение». 
       Создать в объекте «Приложение», вложенные объекты, «Заголовок, Тело, Футер, Дата». Создать методы для заполнения и отображения документа. */
       let content = {
        head: "",
        body: "",
        footer: "",
        date: "",
        print:  function (name) {
             document.write("<p>" +"Заголовок "+name+" - "+ this.head);
             document.write("<p>" +"Тело "+name+" - "+ this.body);
             document.write("<p>" +"Футер "+name+" - "+ this.footer);
             document.write("<p>" +"Дата "+name+" - "+ this.date);
         }
    };

    let myDocument = content;
    myDocument.application = content;
    myDocument.append = function () {
         myDocument.head = prompt("Введите заголовок!");
         myDocument.application.head = myDocument.head;
         myDocument.body = prompt("Введите тело!");
         myDocument.application.body = myDocument.body;
         myDocument.footer = prompt("Введите футер!");
         myDocument.application.footer = myDocument.footer;
         myDocument.date = prompt("Введите дату!");
         myDocument.application.date = myDocument.date;
     };

     myDocument.append();
     myDocument.print("документа");
     myDocument.application.print("приложения");