// /*
// Функция-конструктор персоны
// * @{string} name - имя
// * @{number} age - возраст
// */
// function Human(name, age) {
//     this.name = name;
//     this.age = age;
// };
// let Pavel = new Human('Pavel', 29);
// let Aleksandr = new Human('Aleksandr', 22);
// let Masha = new Human('Masha', 74);
// let Aleksey = new Human('Aleksey', 25);
// let Valeriya = new Human('Valeriya', 36);
// //массив всех персон
// const peoples = [Pavel, Aleksandr, Masha, Aleksey, Valeriya];
// /*
// Функция сортировки по возрасту
// * @{object} mas - массив персон
// */
// function sortByAge(mas) {
//  	//Проверка параметра на массив
// 	if (mas instanceof Array) { 
//         let sortedData = mas.sort(func)
//         return sortedData;
//     } else console.log('Error');  
// 	function func(a, b) {
//         return a.age - b.age;
//     };
// };
// /*
// * Функция сортировки по имени
// * @{object} mas - массив персон
// */
// function sortByName(mas) {
//  	//Проверка параметра на массив
// 	if (mas instanceof Array) { 
//         let sortedData = mas.sort(func)
//     	return sortedData;
//     } else console.log('Error'); 
//     function func(a, b) {
//         return a.name.toUpperCase() > b.name.toUpperCase();
//     };
// };
// /*
// * Функция рендера данных
// * @{object} mas - массив объектов
// * @{string} key - item объекта
// */
// function render(mas, item) {
//     mas.forEach(show);
//     function show(el, i) {
//         document.body.textContent +=' '+(el[item]);
//     };
// };
 
// render(sortByName(peoples), 'name');
// render(sortByAge(peoples), 'age');



/*
Вторая часть задания
*/

function Human(name, position, workingHours) {
    this.name = name;
    this.position = position;
    this.workingHours = workingHours;
	this.workToday = function () {
		document.write('Сегодня работает: ' + this.name);
	};
};

let vasya = new Human('Vasya', 'manager', 'full-time');
vasya.typeOfWork = 'remote';
vasya.workToday();