/*
Создайте массив styles с элементами «Джаз» и «Блюз».
Добавьте «Рок-н-ролл» в конец.
Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
Удалите первый элемент массива и покажите его.
Вставьте «Рэп» и «Регги» в начало массива.
*/

let styles = ['Джаз', 'Блюз'];
alert(styles);
styles.push('Рок-н-Ролл');
alert(styles);
styles[Math.floor(styles.length/2)] = 'Классика';
alert(styles);
styles.shift();
alert(styles);
styles.unshift('Рэп', 'Регги');
alert(styles);