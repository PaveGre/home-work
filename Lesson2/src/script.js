/*
1. Перепишите оператор if с использованием оператора '?'

var a = parseInt(prompt('Введите a'));
var b = parseInt(prompt('Введите b'));

if (a + b < 4) {
    result = 'Мало';
} else {
    result = 'Много';
}

alert(result);
*/

var a = parseInt(prompt('Введите a'));
var b = parseInt(prompt('Введите b'));

result = (a + b < 4) ? 'Мало' : 'Много';

alert(result);

/*
2. Перепишите if...else с использованием нескольких операторов '?'

login = prompt('Веддите логин');

var message;

if (login == 'Вася') {
    message = 'Привет';
} else if (login == 'Директор') {
    message = 'Здравствуйте';
} else if (login == '') {
    message = 'нет логина';
} else {
    message = '';
}

alert(message);
*/

login = prompt('Введите логин');

var message = (login == 'Вася') ? 'Привет' :
(login == 'Директор') ? 'Здравствуйте' :
(login == '') ? 'нет логина' :
'';

alert(message);

/*
3. Дано два числа A и B где (A<B). Выведите на экран сумму всех чисел, расположенных в числовом промежутке от A до B. 
Выведите на экран все нечетные значения, расположенные в числовом промежутке от A до B.
*/

var a = parseInt(prompt("Введите число A"));
var b = parseInt(prompt("Введите число B"));

if (a < b) {
    var i = a; 
    var sum = 0;
    var list = "";
    while (i < b){
        sum+=i;
        if (i % 2 !== 0) {
            list += " " + i;
        }

        i++;
    }
    document.write("Сумма чисел от А до В равна "+ sum);
    if (list.length>0){
        document.write("<p>"+"В числовом промежутке от А до В найдены не четные числа:"+ list);
    }
    else{
        document.write("<p>"+"В числовом промежутке от А до В не найдены не четные числа!");
    }
    
}
else{
    document.write("Условие А < В не выполняется!"+"<br>")
}

/*
4. Используя циклы нарисуйте в браузере с помощью пробелов и звездочек: *Прямоугольник *Прямоугольный треугольник *Равносторонний треугольник *Ромб
*/

// (1) Прямоугольник

document.write("<hr>");

const freestar = '*';
const freespace = '&nbsp;';
const br = '<br/>';
let str = '';

for (let i = 0; i < 20; i++) {
    document.write(freespace + freestar);
    if (i == 19) {
        document.write('<br/>');
    }
}

for (let i = 0; i < 6; i++) {
    for (let i = 0; i < 30; i++) {
        if (i == 0 || i == 29) {
            document.write(freestar);
        } else if (i > 1 || i < 29) {
            document.write(freespace + freespace);
        }
    }
    document.write('<br/>');
}

for (let i = 0; i < 20; i++) {
    document.write(freespace + freestar);
    if (i == 19) {
        document.write('<br/>');
    }
}
document.write("<hr>");

// (2) Прямоугольный треугольник

var max = 8;
for (var h = 1; h < 9; h++) {

for (var w = max--; w < 9; w++) {
document.write("*\n");
}
document.write("<br>");
}
document.write("<hr>");

// (3) Равносторонний треугольник

var line = 8; 
var space = 7; 
var star = 1; 

for (var h = 0; h < line; h++) {
for (var wsp = 0; wsp < space; wsp++) {
document.write("&nbsp\n");
}
for (var wst = 0; wst < star; wst++) {
document.write("*");
}
space -= 1;
star += 2;
document.write("<br>");
}
document.write("<hr>");

// (4) Ромб

var space = 18;
var star = 1;
var line = 20;
for (var i = 0; i <= line; i++){
    for (var j = 0; j <= space; j++){
        document.write("&nbsp")
    }
    for (var k = 0; k < star; k++){
        document.write("*")
    }
    space--;
    star++;
    if (i >= line / 2 && i <= line){
        star = star - 2;
        space = space + 2;
    }
    document.write("<br>");
}
document.write("<hr>");